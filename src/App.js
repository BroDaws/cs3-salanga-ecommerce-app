/* src > App.js */

import React, {useState} from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import UserContext from './UserContext.js';

// App Components
import AppNavbar from './components/AppNavbar.js';
/*import TreatmentView from './components/TreatmentView';*/

// Page Components
import Home from './pages/Home';
import Treatment from './pages/Treatment';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';




export default function App() {



const [ user, setUser ] = useState( { accessToken: localStorage.getItem('access') } );

	const unsetUser = () => {
		//localStorage.clear() - to clear every information that is stored inside the localStorage
		localStorage.clear();
		setUser( { access: null } )
	}

	return (
	  <UserContext.Provider value={{ user, setUser, unsetUser }}>
		<Router>
    		<AppNavbar user={user} />

    		<Routes>
      			<Route path = "/"element = {<Home/>} />
      			<Route path = "/treatments" element = {<Treatment />} />
      			<Route path = "/reguser" element = {<Register />} />
      			<Route path = "/veruser" element = {<Login />} />
      			<Route path= "/logout" element={<Logout/>}/>
      			<Route path = "*" element = {<Error />} /> 

    		</Routes>

  		</Router>
  	</UserContext.Provider>
	)


}
















