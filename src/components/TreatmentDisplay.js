/* src > components > TreatmentDisplay.js */

/* !!!! ADDED ON 10JUN2022  */


import UserContext from '../../UserContext';
import { useState, useContext } from 'react';
import { Link } from 'react-router-dom';

export default function TreatmentDisplay({description, name, price, treatmentId, onClick}) {

	const { user } = useContext(UserContext);
	const [quantity, setQuantity] = useState(1);

	const increaseQuantity = () => {
		setQuantity(quantity + 1);
	}

	const decreaseQuantity = () => {
		if(quantity > 1) {
			setQuantity(quantity - 1);
		}
	}

	return (
		<div>

			<div>
				<h2>{name}</h2>
				<h5>${price}</h5>
				<p>{description}</p>
				<span>
					<button type="button" className="quantity-btn" onClick={increaseQuantity}>+</button>
					<p className="quantity-text">{quantity}</p>
					<button className="quantity-btn" onClick={decreaseQuantity}>-</button>
				</span>
				{	user.id !== null 
					?
					<button className="cart-btn" onClick={() => onClick(treatmentId, quantity)}>ADD TO CART</button>
					:
					<Link to="/veruser"><button className="cart-btn">ADD TO CART</button></Link>
				}
			</div>
		</div>
	)
}

