/* src > components > UserView.js */

import React, {useState, useEffect} from 'react';

import{	Container } from 'react-bootstrap';

import TreatmentCard from "./TreatmentCard";


export default function UserView({treatmentsData}) {



	const [treatments, setTreatments] = useState([]);

	useEffect(() => {
		
		const treatmentsArr = treatmentsData.map(treatment => {

			if (treatment.isActive === true){
			
				return (
					<TreatmentCard treatmentProp={treatment} key={treatment._id}/>
					
					)

			} else {
				return null;
			}
			
		});
		
		setTreatments(treatmentsArr);

	}, [treatmentsData]);


	return (
		<Container>
			{treatments}
		</Container>
	);
}
