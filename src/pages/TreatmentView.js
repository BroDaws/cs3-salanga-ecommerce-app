/* src > pages > TreatmentView.js */

/* !!!! ADDED ON 10JUN2022  */

import { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import ProductDisplay from '../components/TreatmentDisplay';
import Swal from 'sweetalert2';

export default function TreatmentView() {

	const history = useNavigate();
	const { treatmentId } = useParams();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	

	const addToCart = (treatmentId, quantity) => {
		// https://lit-plateau-63513.herokuapp.com
		// fetch('http://localhost:4000/api/carts/', {
		fetch('https://lit-plateau-63513.herokuapp.com/api/carts/', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				treatmentId: treatmentId,
				quantity: quantity,

			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				Swal.fire({
					  title: 'Successfully Booked',
					  text: `(${quantity}) ${name} added to cart`,

					})

				history('/treatments');
			}
			else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: ""
				})
			}
		})
	}

	useEffect(() => {

		// https://lit-plateau-63513.herokuapp.com
		// fetch(`http://localhost:4000/api/treatments/${treatmentId}`)
		fetch(`https://lit-plateau-63513.herokuapp.com/api/treatments/${treatmentId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})
	}, [treatmentId]);

	return (
		<div>
			<TreatmentDisplay 
			description={description}
			price={price}
			treatmentId={treatmentId}
			name={name}
			onClick={addToCart}
			/>
		</div>
		)
}



//=====================================================================

