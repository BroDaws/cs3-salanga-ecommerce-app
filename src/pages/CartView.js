/* src > pages > CartView.js */

/* !!!! ADDED ON 10JUN2022  */

import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { Badge } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CartView() {

	const { user } = useContext(UserContext);
	const [treatments, setTreatments] = useState([]);
	const [bill, setBill] = useState(0);
	const [cartId, setCartId] = useState('');
	const token = localStorage.getItem('token');
	const checkoutBtnStyle = {padding: '8px 20px', borderRadius: '2px', 
							border: '1px solid', backgroundColor: '#fff' 
							};


	const placeOrder = (cartId) => {
		//fetch('http://localhost:4000/api/orders/', {
		fetch('https://lit-plateau-63513.herokuapp.com/api/orders/', {
			method: "POST",
			headers: {
				Authorization: `Bearer ${token}`
			}
			})
			.then(res => res.json())
			.then(data => {
				if(data !== false) {
					setTreatments([]);
					setBill(0);

					Swal.fire({
						title: 'You successfully booked an appointment.',
						icon: 'success',
						text: 'Thank you for the purchase.'
					})
				}
				else {
					Swal.fire({
						title: 'Something went wrong',
						icon: 'error',
						text: 'Please try again.'
					})
				}
		})
	}

	const removeCartItem = (treatmentId) => {
		// https://lit-plateau-63513.herokuapp.com
		//fetch(`http://localhost:4000/api/carts/${treatmentId}`, {
		fetch(`https://lit-plateau-63513.herokuapp.com/api/carts/${treatmentId}`, {
			method: 'DELETE',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setTreatments(data.treatments);
		})

	}

	useEffect(() => {
		//fetch('http://localhost:4000/api/carts/', {
		fetch('https://lit-plateau-63513.herokuapp.com/api/carts/', {
			method: "GET",
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.length > 0) {
				setTreatments(data[0].treatments);
				setBill(data[0].bill);
				setCartId(data[0]._id)
			}
		})
	}, [token])

	return (
		(user.id === null)
		?
		<Navigate to="/veruser"/>
		:
		<div className="my-5">
			{treatments.map((treatment) => 
				<div className="col-md-6 mx-auto" key={treatment.treatmentId}>
					<div className="d-flex justify-content-between px-3">
						<div className="d-flex">
							
							<div>
								<div>{treatment.name}</div>
								<p>PRICE</p>
								<Badge>QUANTITY</Badge>
							</div>
						</div>
						<div>
							<div onClick={() => removeCartItem(treatment.treatmentId)}>Delete</div>
							<p>${treatment.price}</p>
							<Badge bg="light" text="dark" pill>{treatment.quantity}</Badge>
						</div>
					</div>
					<hr/>
				</div>
				)
			}
			<div className="col-md-6 mx-auto mt-3 px-3">
				<div className="d-flex">
					<strong className="me-auto px-3">Total: </strong>
					<h3 className="px-3">${bill}</h3>
				</div>
			</div>
			<div className="col-md-6 mx-auto px-3">
				<button type="button" style={checkoutBtnStyle} onClick={() => placeOrder(cartId)}>Checkout</button>
			</div>
		</div>
	)
}
