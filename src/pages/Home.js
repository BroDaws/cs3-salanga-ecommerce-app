/* src > pages > Home.js */

import { Fragment } from 'react'
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = { 
		title: "Therapy & Rehab Solutions",
	    content: "Getting back on your feet is our priority.",
	    destination: "/treatments",
	    label: "Request an Appointment!"
	}
//========================
	const myStyle={
        backgroundImage: 
 "url('https://images.pexels.com/photos/3756042/pexels-photo-3756042.jpeg')",

        height:'100vh',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat'
    };
//========================
	return(
		<Fragment>
			<div style={myStyle}>	
				<Banner data={data}/>
				<Highlights/>
			</div>
		</Fragment>
	)


}

//=======================================





