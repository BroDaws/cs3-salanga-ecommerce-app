/* src > pages > Login.js */

import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'


export default function Login(){

    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);
    //const [ isDisabled, setIsDisabled ] = useState(true);




    function authenticate(e) {
        e.preventDefault();

            fetch('https://lit-plateau-63513.herokuapp.com/api/users/veruser', {
            //fetch('http://localhost:4000/api/users/veruser', {

                method: "POST",
                headers: {'Content-Type' : 'application/json'},
                body: JSON.stringify({
                    email: email,
                    password: password
                    })
            })

        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(typeof data.access !== "undefined"){

                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access)

                Swal.fire({
                    title: 'Login Successful',
                    icon: 'success',
                    text: 'Welcome to Therapy and Rehab Solutions!'
                });
            } else {
                Swal.fire({
                    title: 'Authentication Failed',
                    icon: 'error',
                    text: 'Please check your credentials'
                });
            };
        })

        setEmail('');
        setPassword('');

        const retrieveUserDetails = (token) => {
            fetch('https://lit-plateau-63513.herokuapp.com/api/users/details', {
            //fetch('http://localhost:4000/api/users/details', {

                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });
            })
        };

    }

  useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);
   



    return(
        
        (user.id !== null) ?
            <Navigate to= "/treatments"/>
        :

            <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => 
                        setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => 
                        setPassword(e.target.value)} required/>
                </Form.Group>
            
 
            
            { isActive ?
                <Button variant="primary" type="submit" id="submitBtn" className="mt-3 mb-3">
                Login
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-3" disabled>
                Login
                </Button>
            }
            </Form>
        
    );
};

